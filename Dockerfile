# Header
FROM ubuntu:latest

# Install dependencies
RUN apt-get update && apt-get install -y \
	python \
	python-pip \
	python-setuptools

# Copy in source code
COPY . /src/

WORKDIR /src/

RUN python setup.py install

# Use the default config file
RUN mv /src/config/config.ini.default /src/config/config.ini

